/*
 * SimpleTableModel.java
 *
 * Created on 26 de septiembre de 2007, 22:59
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package Ejercicio0703Tabla;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author aaparicio
 */
public class SimpleTableModel extends AbstractTableModel {
    ArrayList datos = new ArrayList();
    String [] columnas = {"Nombre", "Apellidos", "Edad", "Deporte", "Vegetariano"};
    Class[] types = new Class [] {
        java.lang.String.class, java.lang.String.class, java.lang.Integer.class,
        java.lang.String.class, java.lang.Boolean.class};

    /** Creates a new instance of SimpleTableModel */
    public SimpleTableModel() {
        //Creamos los objetos de una fila
        Object [] fila = new Object[6];
        fila[0] = "Alberto";
        fila[1] = "Aparicio vila";
        fila[2] = new Integer(29);
        fila[3] = "BASKET";
        fila[4] = new Boolean(false);
        
        //El array list contendra un array de Object[] en cada fila
        datos.add(fila);
    }
    
    
    /**
     * Devuelve el nombre de la columna
     * @param col Indice de columna
     * @return Nombre de la columna
     */
    public String getColumnName(int col) { 
        return columnas[col].toString(); 
    }
    /**
     * Devuelve el numero de filas
     * @return numero de filas
     */
    public int getRowCount() { return datos.size(); }
    
    /**
     * Devuelve el numero de columnas 
     * @return numero de columnas
     */
    public int getColumnCount() { return columnas.length; }
    
    /**
     * Devuelve el valor del objeto en la fila y columna
     * @param row fila a buscar
     * @param col columna a buscar
     * @return valor del objeto
     */
    public Object getValueAt(int row, int col) { 
        Object[] fila = (Object[]) datos.get(row);
        return fila[col];
    }
    /**
     * Devuelve la clase que corresponde al tipo de columna
     * @param columnIndex columna 
     * @return Clase tipo
     */
    public Class getColumnClass(int columnIndex) {
        return types[columnIndex];
   }

    /**
     * Determina si una fila y columna ha de ser editable
     * @param row fila de la tabla
     * @param col columna de la tabla
     * @return valor booleano indicando si es o no editable
     */
    public boolean isCellEditable(int row, int col) { return true; }
    
    /**
     * Actualiza un objeto de una fila y columna
     * @param value Objeto a actualizar
     * @param row fila de la tabla
     * @param col columna de la tabla
     */
    public void setValueAt(Object value, int row, int col) {
        Object []  fila = (Object []) datos.get(row);
        fila[col] = value;
        fireTableCellUpdated(row, col);
    }    
    
    /**
     * A�ade una fila al modelo
     * @param fila fila a a�adir 
     */
    public void addRow(Object [] fila) {
        datos.add(fila);
        fireTableDataChanged();

    }
    
    /**
     * Elimina una fila del modelo
     * @param fila indice de la fila a eliminar
     */
    public void removeRow(int fila) {
        datos.remove(fila);
        fireTableDataChanged();
    }
}
