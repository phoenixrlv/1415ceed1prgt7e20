/*
 * ColorRenderer.java
 *
 * Created on 1 de octubre de 2007, 1:15
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package swing12;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author pacoaldarias
 */
public class ColorRenderer extends JLabel implements TableCellRenderer{
    
    /** Creates a new instance of ColorRenderer */
    public ColorRenderer() {
    }
    
    /**
     * Devuelve un componente para personalizar la celda
     * @param table Componente JTable padre
     * @param value Valor de la celda
     * @param isSelected True si la celda est� seleccionada
     * @param hasFocus Si tiene el foco
     * @param row Indice de la fila
     * @param column Indice de la columna
     * @return Componente para ser visualizado
     */
    public Component getTableCellRendererComponent(JTable table, Object value, 
            boolean isSelected, boolean hasFocus, int row, int column) {
            
            this.setBackground(new Color(184,207,229));
            
            //Modificar el Renderer para que en funcion de la edad
            //lo pinte de un color u otro
            int edad = ((Integer)value).intValue();
            
            if ( (edad>=0) && (edad<=15) ) {
                this.setForeground(Color.RED);
            } 
            
           if ( (edad>=15) && (edad<=30) ) {
                this.setForeground(Color.GREEN);
            } 
           
           if ( edad>30 ) {
                this.setForeground(Color.BLUE);
            } 
            
            this.setHorizontalAlignment(SwingConstants.RIGHT);
            this.setText(""+value);
            this.setToolTipText("El valor de esta celda es: "+value);
            if (isSelected) this.setOpaque(true);
            else this.setOpaque(false);
            
            
            
            
            return this;
    }
}
