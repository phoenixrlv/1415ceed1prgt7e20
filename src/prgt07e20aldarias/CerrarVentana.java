/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package prgt07e20aldarias;

/**
 * Fichero: CerrarVentana.java
 * @date 10-mar-2014
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */


import javax.swing.JOptionPane;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import javax.swing.JLabel;

public final class CerrarVentana extends JFrame {

  public int sino(){
    
  int seleccion = JOptionPane.showOptionDialog(
   this,
   "Salir", // Mensaje
   "Selector de opciones", // Titulo ventana
   JOptionPane.YES_NO_OPTION, //Opciones queremos que tenga la ventana
   JOptionPane.QUESTION_MESSAGE, // Determine qué icono mostrar
   null,    // null para icono por defecto.
   new Object[] { "si", "no" },   // null para YES, NO y CANCEL
   "si"); // Por defecto
   return seleccion;
   
}
  
   public void defineCerrar(){
     addWindowListener(new WindowAdapter() {
       public void windowClosing(WindowEvent e) {
          int res;
          res = sino(); 
          if ( sino() == 0 ) {
           System.exit(0);
         }
     }
     });
    }
  
  public CerrarVentana() {
    super("Hola Mundo"); // Titulo  
    JLabel label = new JLabel("Hola Mundo");
    this.getContentPane().add(label);
    setSize(200, 100);
    setLocationRelativeTo(null); // Centrar 
    setVisible(true);
    defineCerrar();
  }

  public static void main(String[] args) {
    new CerrarVentana();
  }
}
